var Baseball_Bat_Pack = function () {
    var _ = this;

    _.object = null;
    _.name = 'baseball_bat_pack';
    _.format = 'gltf';
    _.control = {
        uiActivated: false,
        x: false,
        y: false,
        z: false,
        currindex: 0,
        currentAnimation: 'default',
        speed: 0.01
    }
    _.animations = {
        initialposition: [
            {
                position: { x: 0, y: 0.35, z: -2}
            }
        ],
        default: [
            {
                rotation: { x: 0, y: 0, z: 0 },
                //position: { x: 1, y: 3, z: 5}
            }
        ],
        pullback: [
            {
                rotation: { x: 0, y: 0, z: 0 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: -0.3, y: -0.5, z: -0.1 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: -0.3, y: -1, z: -1 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: -0.5, y: -1, z: -1 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: -1, y: -1, z: -2 },
                //position: { x: 1, y: 3, z: 5}
            }

        ],
        swing: [
            {
                rotation: { x: 0, y: 0, z: 0 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: 0.5, y: 0.5, z: -0.7 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: 0.5, y: 0.5, z: -1.2 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: 1, y: 1, z: -2 },
                //position: { x: 1, y: 3, z: 5}
            },
            {
                rotation: { x: 1, y: 1.5, z: -2 },
                //position: { x: 1, y: 3, z: 5}
            }
        ]
    }

}