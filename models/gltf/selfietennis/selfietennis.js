var Selfietennis = function () {
    var _ = this;

    _.object = null;
    _.name = 'selfietennis';
    _.format = 'gltf';
    _.control = {
        uiActivated: false,
        x: false,
        y: false,
        z: false,
        currindex: 0,
        currentAnimation: 'initial',
        speed: 0.01
    }
    _.animations = {
        default: [
            {
                position: { x: 0, y: 0, z: 0 },
                //position: { x: 1, y: 3, z: 5}
            }
        ],
        initial: [
            {
                position: { x: 0, y: 0.35, z: 0 }
            },
            {
                position: { x: 0, y: 0.35, z: 3 }
            }
        ],
        //Pitches
        fastball: [
            {
                position: { x: 0, y: 0.35, z: -0.5 }
            },
            {
                position: { x: 0, y: 0.35, z: -3 }
            }
        ],
        breakingball: [
            {
                position: { x: 0, y: 0.35, z: 3 }
            },
            {
                position: { x: 0, y: 0.35, z: 2.5 }
            },
            {
                position: { x: 0, y: 0.30, z: 2 }
            },
            {
                position: { x: 0, y: 0.25, z: 1 }
            },
            {
                position: { x: 0, y: 0.2, z: 0.5 }
            },
            {
                position: { x: 0, y: 0.1, z: -3 }
            }

        ],
        changeup: [
            {
                position: { x: 0, y: 0.35, z: 3 }
            },
            {
                position: { x: 0, y: 0.35, z: -3 }
            }
        ],
        curve: [
            {
                position: { x: 0, y: 0.35, z: 3 }
            },
            {
                position: { x: 0, y: 0.35, z: 2 }
            },
            {
                position: { x: -0.05, y: 0.35, z: 1 }
            },
            {
                position: { x: -0.1, y: 0.35, z: 0 }
            },
            {
                position: { x: -0.15, y: 0.35, z: -2 }
            },
            {
                position: { x: -0.2, y: 0.35, z: -3 }
            },
        ],
        scale: [
            {
                scale: { x: 0.2, y: 0.2, z: 0.2 }
            }
        ]
    }

}