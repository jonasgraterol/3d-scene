describe("Modules", function() {

    //Instantiate 3DModels objects
    var Baseball_Bat = require('../../models/gltf/baseball_bat/baseball_bat.js');
    var baseball_bat;
    
    beforeEach(function(){
        baseball_bat = new Baseball_Bat();
    });
        
    describe("getTemplateData", function(){
        
        it("should include the controls and animations attributes", function(){
            expect(typeof(baseball_bat.control)).toEqual('object');
            expect(typeof(baseball_bat.animations)).toEqual('object');
        });

    });

});