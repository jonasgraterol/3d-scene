/* DESCRIPTION
 *   A reusable prototype to handle creation and set up for Scene and 3D objects at UI
 *      Dependencies: three.js and jQuery
 *
 ******************************************************************************/
var ThreeDObjectLoader = function () {
    var _ = this;

    //Variables used only internally
    var container, controls;
    var camera, scene, renderer;
    var animationSystem;

    //Declaration of protoype vars and methods
    _.targetContainer = 'body';
    _.objectCollection = new Map();
   
    //FUNCTION TO INITIALIZE THE three.js LOADER
    /*
    * Must be called after the DOM is ready
    */
    _.init = function () {
        container = document.createElement('div');
        $(`${_.targetContainer}`).append(container);
        camera = new THREE.PerspectiveCamera(10, window.innerWidth / window.innerHeight, 0.1, 10000);
        camera.position.set(1, 5, 20);

        controls = new THREE.TrackballControls(camera);
        scene = new THREE.Scene();
        scene.background = new THREE.Color(0xcce0ff);
        scene.fog = new THREE.Fog(0xcce0ff, 500, 10000);
        scene.add(new THREE.HemisphereLight());

        //Add a grid helper
        var gridHelper = new THREE.GridHelper(10, 20);
        scene.add(gridHelper);

        var directionalLight = new THREE.DirectionalLight(0xffeedd);
        directionalLight.position.set(0, 0, 2);
        scene.add(directionalLight);

        //Case the model type
        /*
        * NOTE: "objectCollection" is a Map containing all 3D objects that should be loaded at init()
        */
        _.objectCollection.forEach((value, key) => {
            _.runLoader(value);
        });

        //define object animation
        animationSystem = new AnimationSystem();

        renderer = new THREE.WebGLRenderer({
            logarithmicDepthBuffer: true
        });
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
        container.appendChild(renderer.domElement);
        window.addEventListener('resize', _.resize, false);
    }

    //FUNCTION ADJUST THE SCENE TO SCREEN DIMENSIONS
    /*
    * Attached to resize event
    */
    _.resize = function () {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    //RUN ANIMATIONS FUNCTION
    /*
    * Must be called after init() in the UI
    */
    _.animate = function () {
        requestAnimationFrame(_.animate);
        controls.update();
        renderer.render(scene, camera);

        //Run animation system responding to user events from the UI
        _.objectCollection.forEach((value, key) => {
            if (value.object != null) {
                if (value.control.x == true || value.control.y == true || value.control.z == true) {
                    animationSystem.runArrayAnimation(value.object, value.animations[value.control.currentAnimation], value.control);
                }
            }
        })
    }

    //REMOVE OBJECTS FROM THE SCENE
    /*
    * @param {Mesh object} "obj": same type of object added to the Scene in runLoader() function
    */
    _.removeObject = function (obj) {
        scene.remove(obj);
        _.animate();
    }

    //LOADS AN 3D OBJECT TO THE ESCENE
    /*
    * @param {custom object} "mod": custom object that contains following attributes (Refer to any models/gltf/MODEL_NAME/MODEL_NAME.js for a example)
    *       {string} "mod.name": name, must be unique in the current UI
    *       {string} "mod.format": format of the 3D object (Accepted values: [3ds, gltf])
    *       {object} "mod.control": contains attibutes to control the object animations
    *       {object} "mod.animations": a object with animations sequences that can be runned for AnimationSystem module
    * @return none: but 3D object is added into the Scene and into _.objectCollection as well
    */
    _.runLoader = function (mod) {
        
        switch (mod.format) {
            case '3ds':
                //3ds files dont store normal maps
                var loader = new THREE.TextureLoader();
                var normal = loader.load(`models/3ds/${mod.name}/textures/drevopalka.jpg`);
                var loader = new THREE.TDSLoader();
                loader.setPath(`models/3ds/${mod.name}/textures/`);
                loader.load(`models/3ds/${mod.name}/${mod.name}.3ds`, function (object) {
                    object.traverse(function (child) {
                        if (child instanceof THREE.Mesh) {
                            console.log('Mesh');
                            console.log(child);
                            child.material.normalMap = normal;
                        }
                    });
                    mod.object = gltf.scene;
                    _.objectCollection.set(mod.name, mod);

                    scene.add(_.objectCollection.get(mod.name).object);
                });
                break;
            case 'gltf':
                var loader = new THREE.GLTFLoader();
                loader.load(`models/gltf/${mod.name}/${mod.name}.gltf`,
                    // called when the resource is loaded
                    function (gltf) {

                        gltf.scene.traverse(function (child) {
                            if (child.isMesh) {
                                console.log(child);
                            }
                        });
                        mod.object = gltf.scene;
                        _.objectCollection.set(mod.name, mod);

                        scene.add(_.objectCollection.get(mod.name).object);

                    },
                    // called while loading is progressing
                    function (xhr) {

                        console.log((xhr.loaded / xhr.total * 100) + '% loaded');

                    },
                    // called when loading has errors
                    function (error) {

                        console.log('An error happened');
                        console.log(error);

                    });
                break;

            default:
                var loader = new THREE.TextureLoader();
                var normal = loader.load(`models/3ds/${mod.name}/textures/drevopalka.jpg`);
                var loader = new THREE.TDSLoader();
                loader.setPath(`models/3ds/${mod.name}/textures/`);
                loader.load(`models/3ds/${mod.name}/${mod.name}.${mod.format}`, function (object) {
                    object.traverse(function (child) {
                        if (child instanceof THREE.Mesh) {
                            console.log('Mesh');
                            console.log(child);
                            child.material.normalMap = normal;
                        }
                    });
                    mod.object = gltf.scene;
                    _.objectCollection.set(mod.name, mod);

                    scene.add(_.objectCollection.get(mod.name).object);
                });
                break;
        }
    }
}