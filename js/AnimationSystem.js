/* DESCRIPTION
 *   A customaized animation system to manipulate 3D objects into a Scene using the Euler tridimentional plane as paradig
 *
 ******************************************************************************/
var AnimationSystem = function () {
    var _ = this;

    //RUN ALL PENDING ANIMATIONS OF A 3D OBJECT
    /*
    * Should be called inside an animate loop (Recommended)
    * @param {custom object} "mod": custom object that contains following attributes (Refer to any models/gltf/MODEL_NAME/MODEL_NAME.js for a example)
    *       {string} "mod.name": name, must be unique in the current UI
    *       {string} "mod.format": format of the 3D object (Accepted values: [3ds, gltf])
    *       {object} "mod.control": contains attibutes to control the object animations
    *       {object} "mod.animations": a object with animations sequences that can be runned for AnimationSystem module
    * @param {array[obj]} "arr": An array of animations. Is the same than "mod.animations"
    * @param {array[obj]} "axisAnimate":  An array of control for object animations. Is the same than "mod.control"
    */
    _.runArrayAnimation = function (obj, arr, axisAnimate) {
        if (arr[axisAnimate.currindex].hasOwnProperty('rotation')) {
            _.handleAnimation(obj, 'x', 'rotation', arr[axisAnimate.currindex].rotation.x, arr, axisAnimate);
            _.handleAnimation(obj, 'y', 'rotation', arr[axisAnimate.currindex].rotation.y, arr, axisAnimate);
            _.handleAnimation(obj, 'z', 'rotation', arr[axisAnimate.currindex].rotation.z, arr, axisAnimate);
        }
        if (arr[axisAnimate.currindex].hasOwnProperty('position')) {
            _.handleAnimation(obj, 'x', 'position', arr[axisAnimate.currindex].position.x, arr, axisAnimate);
            _.handleAnimation(obj, 'y', 'position', arr[axisAnimate.currindex].position.y, arr, axisAnimate);
            _.handleAnimation(obj, 'z', 'position', arr[axisAnimate.currindex].position.z, arr, axisAnimate);
        }
        if (arr[axisAnimate.currindex].hasOwnProperty('scale')) {
            _.handleAnimation(obj, 'x', 'scale', arr[axisAnimate.currindex].scale.x, arr, axisAnimate);
            _.handleAnimation(obj, 'y', 'scale', arr[axisAnimate.currindex].scale.y, arr, axisAnimate);
            _.handleAnimation(obj, 'z', 'scale', arr[axisAnimate.currindex].scale.z, arr, axisAnimate);
        }
    }

    //FUNCTION TO HANDLE SPECIFIC ANIMATIONS
    /*
    * Modifies 3D object displaying porperties as POSITION, ROTATION and SCALE 
    * @param {3D object} "objecttoanimate": A 3D object 
    * @param {string} "axis": Axis to be modified or animation applied. Allowed values [x, y, z]
    * @param {string} "property": The 3D object property to be modified. Allowed values [rotation, position, scale]
    * @param {float} "pointValue": The limit point until the property.axis must stop to be modified
    * @param {array[obj]} "arr": An array of animations. Is the same than "mod.animations"
    * @param {array[obj]} "axisAnimate":  An array of control for object animations. Is the same than "mod.control"
    */
    _.handleAnimation = function (objecttoanimate, axis, property, pointValue, arr, axisAnimate) {
        if (axisAnimate[axis]) {
            if (_.isBigger(pointValue, objecttoanimate[property][axis])) {
                objecttoanimate[property][axis] += axisAnimate.speed;
                if (objecttoanimate[property][axis] >= pointValue) {
                    axisAnimate[axis] = false;
                    console.log(`[${axisAnimate.currindex}].${axis} STOPED`);
                }
            }
            else {
                objecttoanimate[property][axis] -= axisAnimate.speed;
                if (objecttoanimate[property][axis] <= pointValue) {
                    axisAnimate[axis] = false;
                    console.log(`[${axisAnimate.currindex}].${axis} STOPED`);
                }
            }
           
            if (!axisAnimate.x && !axisAnimate.y && !axisAnimate.z && axisAnimate.currindex < arr.length - 1) {
                axisAnimate.currindex++;
                axisAnimate.x = true;
                axisAnimate.y = true;
                axisAnimate.z = true;
                console.log(`New index [${axisAnimate.currindex}] `);
                console.log(arr[axisAnimate.currindex]);
            }
        }
    }

    //FUNCTION THAT COMPARES IF A VALUES IS BIGGER THAN OTHER
    /*
    * Used to decide if a animation shuold go to negative o positive values
    * @param {float} "a": Value to check if is bigger than other one
    * @param {float} "b": Value to be compare
    * @return {boolean} "[true, false]" if a>b returns true else returns false
    */
    _.isBigger = function (a, b) {
        return a > b;
    }
}