# Welcome to **3D Baseball Scene** Wiki

Welcome to the project wiki! Here you can find all information related to this project.

## The Problem

**Create a 3D scene with a 3D object and apply texture and animation**

Create a 3D scene and add a baseball bat object to it. Decorate the bat object by applying wooden texture to barrel and knob and tape texture to the grip. Add animation to show the bat pullback and swing action.

Use ThreeJS for 3D implementation. The UI/UX for the application should be polished. 

## Solution 

A development was done to achieve solve the problem using the following technological stack:

* **FrontEnd**:
* HTML5/CSS3 template
* Three.js library
* Jquery

* **BackEnd**:
* NodeJS
* Express framework to serve the app
* Jasmine for testing

* **Server**:
* App hosted at heroku.com

The app is running at [https://threedbaseballscene.herokuapp.com/](https://threedbaseballscene.herokuapp.com/)

### Aspects to pay attention ###
1. The app behavior relies on two main files **js/3DObjectLoader.js** and **js/AnimationSystem.js** which are custom made objects using prototype design pattern to be ready to be cloned an reused.

2. **js/3DObjectLoader.js** is responsible for Scene creation and adding/removing several 3D objects in the UI responding to user events.

3. **js/AnimationSystem.js** was developed under **Three-dimensional plane (Euclidean space)** paradigm to handle very customized animations for each object in the Scene manipulating *position, rotation and scale* attributes of each 3D Object. This custom animation system is totally scalable and is related to each object thru a custom data object for each 3D object that can be found in **model/3D-FORMAT/MODEL-NAME/MODEL-NAME.js**

### Additional Features ###
The development achieves wit all requirements of the proposed problem and has the following additional features:

1. Batting practice: is an option under the sidebar menu that add a baseball ball and enables a Pitch menu (at the bottom/left corner of the page) to throw some pitches to the bat.
2. Use a big bat: change the bat for a bigger one
3. Use triple bat: change the bat object for a triple bat
4. Change the ball: change the baseball ball for a tennis ball

### Improvement Points ###
1. Use more complex sequences in each 3DObject animations array to achieve smoother animations, by example in ball pitches or bat swing.
2. Create API rest to custom endpoints to bring data to the UI. For example, a get request which returns a random pitch to be thrown, or connects to a database to fetch historical data previous to decide which pitch should be thrown.
3. As I'm not a 3D expert, with more time, I would investigate and read some documentation to improve animations and decorations using existing packages or libraries.  
4. Due to time limitations only few test scripts were written, more unit test should be written.



-----------------------------

To clone this repository go ahead and try:

```
$ git clone https://jonasgraterol@bitbucket.org/jonasgraterol/3d-scene.git
```


Enjoy!